package com.example.lks20

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    fun pindah_transaksi(view: android.view.View){
        intent = Intent(this,TranksaksiActivity::class.java)
        startActivity(intent)
    }

    fun pindah_info(view: android.view.View){
        intent = Intent(this,InfoActivity::class.java)
        startActivity(intent)
    }
}