package com.example.lks20

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView

class PaymentActivity : AppCompatActivity() {
    var bayar = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        bayar = intent.getStringExtra("bayar")?.toDouble()!!
        findViewById<TextView>(R.id.totalPayment).text="$bayar"
    }

    fun btnProses(view: android.view.View) {
        val uang = findViewById<EditText>(R.id.jumlahUang).toString().toDouble()
        val kembalian = uang - bayar

        findViewById<TextView>(R.id.kembalian).text="$kembalian"
    }
}