package com.example.lks20

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import java.text.NumberFormat
import java.util.*

class DetailTransaksiActivity : AppCompatActivity() {
    var totalbayar = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_transaksi)

        var pembeli = intent.getStringExtra("pembeli")
        var barang = intent.getStringExtra("barang")
        var harga = 0.0
        harga = intent.getStringExtra("harga").toString().toDouble()
        var jumlah = intent.getStringExtra("jumlah").toString().toDouble()
        var totalharga = harga * jumlah

        var diskon = 0.0
        totalbayar = 0.0
        totalbayar = totalharga
        if (jumlah>=10){
            diskon = totalbayar/50
            totalbayar = totalharga - diskon
        }

        val local = Locale("id","ID")
        val formatter = NumberFormat.getCurrencyInstance(local)

        val hargaformat = formatter.format(totalharga)
        val diskonformat = formatter.format(diskon)
        val totalformat = formatter.format(totalbayar)

        findViewById<TextView>(R.id.buyer).text="$pembeli"
        findViewById<TextView>(R.id.item).text="$barang"
        findViewById<TextView>(R.id.many).text="$jumlah"
        findViewById<TextView>(R.id.totalPrice).text="$hargaformat"
        findViewById<TextView>(R.id.discount).text="$diskonformat"
        findViewById<TextView>(R.id.totalPay).text="$totalformat"
    }

    fun pindah_bayar(view: android.view.View){
        intent = Intent(this,PaymentActivity::class.java)
        intent.putExtra("bayar",totalbayar.toString())
        startActivity(intent)
    }
}