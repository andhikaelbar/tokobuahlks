package com.example.lks20

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import org.w3c.dom.Text

class TranksaksiActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tranksaksi)

        findViewById<Spinner>(R.id.dropdownBarang).onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val barang =findViewById<Spinner>(R.id.dropdownBarang).getSelectedItem().toString()
                var harga = 0.0

                if (barang=="a"){
                    harga = 1000.0
                }else if (barang=="b"){
                    harga = 2000.0
                }else if (barang=="c"){
                    harga = 3000.0
                }else if (barang=="d"){
                    harga = 4000.0
                }else if (barang=="e"){
                    harga = 5000.0
                }else if (barang=="f"){
                    harga = 6000.0
                }else if (barang=="g"){
                    harga = 7000.0
                }else if (barang=="h"){
                    harga = 8000.0
                }else if (barang=="i"){
                    harga = 9000.0
                }else if (barang=="j"){
                    harga = 10000.0
                }

                findViewById<TextView>(R.id.harga).setText(harga.toString())
            }
        }
    }

    fun proses(view: android.view.View) {
        intent = Intent(this,DetailTransaksiActivity::class.java)
        intent.putExtra("pembeli", findViewById<EditText>(R.id.pembeli).text.toString())
        intent.putExtra("barang", findViewById<Spinner>(R.id.dropdownBarang).getSelectedItem().toString())
        intent.putExtra("harga", findViewById<EditText>(R.id.harga).text.toString())
        intent.putExtra("jumlah", findViewById<EditText>(R.id.jumlah).text.toString())
        startActivity(intent)
    }

    fun balik_home(view: android.view.View) {
        intent = Intent(this,HomeActivity::class.java)
        startActivity(intent)
    }
}