package com.example.lks20

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import android.widget.ImageView

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val anim = AnimationUtils.loadAnimation(this,R.anim.animasi)
        val logo =findViewById<ImageView>(R.id.logolks)
        logo.apply {
            animation = anim
        }

        Handler(Looper.getMainLooper()).postDelayed({
            val intent =Intent(this,HomeActivity::class.java)
            startActivity(intent)
            finish()
        },2200)
    }
}